fs = require('fs'),
    http = require('http'),
    _ = require('underscore'),
    url = require('url'),
    spawn = require('child_process').spawn,
    path = require('path');

var RefreshSpeed = 1000;
var dir = '/tmp/stream/';
var LastFileID = 0;
var port = 8055;

function StartProcesses(){
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var child = spawn('raspistill', [
        '-w', 640,  //width
        '-h', 480,  //height
        '-q', 20,  //quality (out of 100)
        '-o', dir+'/pic_%08d.jpg', //path to files 
        '-tl', RefreshSpeed,  //time between shots
        '-t', 9999999,  //shots to take
        '-th', 'none'  //thumbnail quality
    ]);
    console.log('raspistill spawned');
}
StartProcesses();
setTimeout(StartServer, 10000);

function StartServer(){
    LastFileID = getMostRecentFileName();
    setTimeout(UpdateLastFileID, RefreshSpeed);
    http.createServer(function (req, res) {
        if (url.parse(req.url).pathname == "/i.jpg" || url.parse(req.url).pathname == "/favicon.ico") {
            var filename = dir + 'pic_'+LastFileID+'.jpg';
            if (!fs.existsSync(filename)) {
                res.writeHead(404);
                res.end();
                LastFileID = getMostRecentFileName();
                return;
            }
            var fileStream = fs.createReadStream(filename);
            fileStream.pipe(res);
        }
        else {
            console.log('New Connection', new Date(), req.headers['user-agent'], req.connection.remoteAddress);
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/html');
            res.end('<html><head><meta http-equiv="refresh" content="3600;"/></head><img style="width:100%" id="cam" /><script>ui();function ui(){var e=document.getElementById("cam");e.src="/i.jpg?d="+new Date,setTimeout(ui,'+RefreshSpeed+')}</script></html>');
        }
    }).listen(port);
    console.log('server running on ',port)
}

function getMostRecentFileName() {
    var files = fs.readdirSync(dir);
    var retval =  _.max(files, function (f) {
        if (f.indexOf('~') > 0){
            return;
        }
        var fullpath = path.join(dir, f);
        try {
            return fs.statSync(fullpath).ctime;
        }
        catch(err) {
            return;
        }
    });
    console.log('had to get last file id and I got', retval);
    if (retval == '-Infinity') return;
    return retval.match(/\d+/)[0];
}

process.on('beforeExit', function(code) { 
  child.kill(); 
} );

function UpdateLastFileID() {
    LastFileID = pad(parseInt(LastFileID) + 1, 8);
    setTimeout(UpdateLastFileID, RefreshSpeed);
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}